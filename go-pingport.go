package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	host := flag.String("host", "", "Target host")
	port := flag.Int("port", 0, "Port to ping")
	count := flag.Int("count", 0, "Stop after <count> replies. Default: infinite")

	timeout := flag.Float64("timeout", 5, "Timeout in seconds")
	delay := flag.Float64("delay", 0, "Delay in seconds")

	portknocking := flag.String("knock", "", "Port knocking sequence")

	flag.Parse()

	target := fmt.Sprintf("%s:%d", *host, *port)

	if *host != "" {
		if *port > 0 {
			fmt.Printf("[+] Target: %s\n", target)
			if *count > 0 {
				for i := 0; i < *count; i++ {
					pinger(target, *port, *timeout)
					if *delay > 0 {
						time.Sleep(time.Duration(*delay) * time.Second)
					}
				}
			} else if *count == 0 {
				for true {
					pinger(target, *port, *timeout)
					if *delay > 0 {
						time.Sleep(time.Duration(*delay) * time.Second)
					}
				}
			} else {
				usage()
			}
		} else if *portknocking != "" {
			fmt.Printf("[+] Target: %s\n", *host)
			fmt.Println("> Port knocking sequence:", *portknocking)
			portKnocker(target, *portknocking, *delay, *timeout)
		}
	} else {
		usage()
	}

}

func usage() {
	fmt.Println("Usage: " + os.Args[0] + " -host <HOST> -port <PORT> [-count <COUNT> -delay <DELAY> -timeout <TIMEOUT>]")
	fmt.Println("Usage: " + os.Args[0] + " -host <HOST> -knock <PORT1,PORT2,PORT3> [-delay <DELAY> -timeout <TIMEOUT>]")
	flag.PrintDefaults()
}

func pinger(target string, portNum int, timeout float64) {
	_, err := net.DialTimeout("tcp", target, time.Duration(timeout)*time.Second)
	if err == nil {
		log.Printf("[+] port open: %d\n", portNum)
	} else {
		log.Printf("[-] port not found: %d\n", portNum)
	}

	time.Sleep(1 * time.Second)

}

func portKnocker(target string, portknocking string, delay float64, timeout float64) {
	list_knock := strings.Split(portknocking, ",")
	for _, port := range list_knock {
		int_port, _ := strconv.Atoi(port)
		pinger(target, int_port, timeout)
		time.Sleep(time.Duration(delay) * time.Second)
	}
}
