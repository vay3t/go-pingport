# Ping Port

Simple ping port checker.

## Build on Linux

### For Linux
```bash
git clone https://gitlab.com/vay3t/go-pingport
cd go-pingport
GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" -trimpath go-pingport.go
upx go-pingport
```

### For Windows
```bash
git clone https://gitlab.com/vay3t/go-pingport
cd go-pingport
GOOS=windows GOARCH=amd64 go build -ldflags "-s -w" -trimpath go-pingport.go
upx go-pingport.exe
```

### For MacOS
```bash
git clone https://gitlab.com/vay3t/go-pingport
cd go-pingport
GOOS=darwin GOARCH=amd64 go build -ldflags "-s -w" -trimpath go-pingport.go
upx go-pingport
```

```bash
git clone https://gitlab.com/vay3t/tiny-go-pingport go-pingport
cd go-pingport
docker build -t go-pingport .
```

## Usage
```
Usage: ./go-pingport -host <HOST> -port <PORT> [-count <COUNT> -delay <DELAY> -timeout <TIMEOUT>]
Usage: ./go-pingport -host <HOST> -knock <PORT1,PORT2,PORT3> [-delay <DELAY> -timeout <TIMEOUT>]
  -count int
        Stop after <count> replies. Default: infinite
  -delay float
        Delay in seconds
  -host string
        Target host
  -knock string
        Port knocking sequence
  -port int
        Port to ping
  -timeout float
        Timeout in seconds (default 5)
```

##### Docker

```bash
docker run -ti --rm go-pingport -count 3 -host TARGET -port PORT
```
