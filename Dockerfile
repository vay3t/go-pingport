FROM golang:1.17-alpine AS builder
RUN apk add --no-cache upx gcc musl-dev
WORKDIR /go/src/go-pingport
ENV GOOS=linux
ENV GOVERSION=1.17
ENV GO111MODULE=auto
ENV GOPATH=/go
ENV PATH=$GOPATH/bin:$PATH
COPY go-pingport.go .
RUN go mod init
RUN go mod tidy
RUN go install -ldflags "-s -w" -trimpath ./...
RUN upx --ultra-brute --lzma -9 --best /go/bin/go-pingport

FROM alpine:3.16.0
WORKDIR /usr/bin/
COPY --from=builder /go/bin/go-pingport .

ENTRYPOINT ["/usr/bin/go-pingport"]
